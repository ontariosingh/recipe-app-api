from django.test import TestCase
from django.contrib.auth import get_user_model


class ModelTests(TestCase):
    def test_create_user_with_email_successful(self):
        email = 'test@email.com'
        password = 'P@ssw0rd'
        user = get_user_model().objects.create_user(
            email=email,
            password=password
        )

        self.assertEqual(user.email, email)
        self.assertEqual(user.check_password(password), True)

    def test_create_user_with_email_normalize(self):
        email = 'test@EMAIL.com'
        password = 'P@ssw0rd'
        user = get_user_model().objects.create_user(
            email=email,
            password=password
        )

        self.assertEqual(user.email, email.lower())

    def test_user_with_invalid_email(self):
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user(None, 'password')

    def test_super_user_successful(self):
        user = get_user_model().objects.create_superuser('email@email.com', 'password')

        self.assertEqual(user.is_staff, True)
        self.assertEqual(user.is_superuser, True)

